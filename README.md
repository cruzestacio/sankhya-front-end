**Container Docker para utilização da ferramenta: Sankhya-Generator** (https://developer.sankhya.com.br/docs/generator-sankhya)

Necessário já ter o docker rodando:

**- Compilar a imagem:**

     docker build -t cruzestacio/snk-generator . 

**- Para facilitar o uso, sugiro criar um alias ou atalho(Linux):**

     alias generator="sudo docker run -it -v $(pwd):/app --rm cruzestacio/snk-generator"

**- No projeto existe um script para criação do atalho(Linux):** 

     chmod a+x create_alias.sh
     ./create_alias.sh

**- Utilização(Linux):**
    Na pasta do projeto chame o atalho:
    
     generator
Se preferir não criar o atalho, pode executar a chamada diretamente na pasta do projeto:

     sudo docker run -it -v $(pwd):/app --rm cruzestacio/snk-generator
