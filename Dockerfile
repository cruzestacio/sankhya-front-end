FROM node:14.19.1

RUN npm init --yes
RUN npm install -g yo --save
RUN npm install -g generator-sankhya --save 

WORKDIR /app

RUN chown -R node /app

USER node

CMD [ "yo","sankhya" ]